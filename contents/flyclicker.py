#Sean Wong
#follow heat box protocol

import numpy as np
import cv2
import pyautogui
from PIL import ImageGrab
import time
import sys

threshold = 100000

#coordinates where buttons are
class Coordinates():
    start = (x,y)
    stop = (x,y)
    fly1 = (x,y)
    fly2 = (x,y)
    fly3 = (x,y)
    fly4 = (x,y)
    laser = (x,y)

def screenShot():
    pyautogui.screenshot('{}.PNG'.format(time.strftime("%H%M%S-%m%d%Y")))

#resizes windeos to correct size for script to work properly
#def resizer():
    #pyautogui.dragTo(x, y, button='left')

def startSession():
    pyautogui.click(Coordinates.start)

def stopSession():
    pyautogui.click(Coordinates.stop)

def sessionE1():
    i = 0   #fly has not moved
    time1 = time.time()
    startSession()
    while time.time() - time1 <= 600:
        screen1 = np.array(ImageGrab.grab(bbox=(0,40,800,640)))
        screen2 = np.array(ImageGrab.grab(bbox=(0,40,800,640)))
        screen_diff = cv2.absdiff(screen1,screen2)
        diff = np.sum(screen_diff)
        if diff >= threshold:
            i = 1   #fly has moved
        else:
            continue
    stopSession()
    return i

def sessionOther():
    i = 0   #laser clicks
    j = 0   #switches only after returning to baseline
    time1 = time.time()
    startSession()
    while i < 20:   #max of 20 clicks
        screen1 = np.array(ImageGrab.grab(bbox=(0,40,800,640)))
        screen2 = np.array(ImageGrab.grab(bbox=(0,40,800,640)))
        screen_diff = cv2.absdiff(screen1,screen2)
        diff = np.sum(screen_diff)
        if diff >= threshold and j == 0:
            pyautogui.click(Coordinates.laser)   #turn laser on
            j = 1   #fly continously moving
            k = 1   #fly has moved
            time.sleep(1)
        elif diff < threshold and j == 1:
            pyautogui.click(Coordinates.laser)   #turn laser off
            i += 1
            j = 0   #fly has stopped
            time1 = time.time()   #reset 8 minute timer
            time.sleep(1)
        elif time.time() - time1 == 480 and j == 0:
            k = 0   #fly has stopped for 8 minutes
            stopSession()
        else:
            continue
    stopSession()
    return k

def renameE1():
    pyautogui.click(Coordinates.fly1)
    pyautogui.typewrite('E1')
    pyautogui.click(Coordinates.fly2)
    pyautogui.typewrite('E1')
    pyautogui.click(Coordinates.fly3)
    pyautogui.typewrite('E1')
    pyautogui.click(Coordinates.fly4)
    pyautogui.typewrite('E1')

def renameOther():
    pyautogui.click(Coordinates.fly1)
    pyautogui.typewrite('R1')
    pyautogui.click(Coordinates.fly2)
    pyautogui.typewrite('T1')
    pyautogui.click(Coordinates.fly3)
    pyautogui.typewrite('R1')
    pyautogui.click(Coordinates.fly4)
    pyautogui.typewrite('N1')

if __name__ == "__main__":
    sessionE1()
    renameOther()
    sessionOther()
    renameE1()
    sessionE1()
    renameOther()
    sessionOther()
    renameE1()
    sessionE1()
    if i == 0 and k == 0:
        sys.exit()
    renameOther()
    sessionOther()
    renameE1()
    sessionE1()
    if i == 0 and k == 0:
        sys.exit()
    renameOther()
    sessionOther()
    renameE1()
    sessionE1()
