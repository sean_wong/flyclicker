import numpy as np
import cv2
from mss.windows import MSS as mss
import time
import psutil

threshold = 100000

def screenDiff():
    x = time.time()
    cpu = []
    mem = []
    file = open('program running.txt', 'w')
    with mss() as sct:
        bbox = {'top':218, 'left':240, 'width':666, 'height':62}
        while time.time() - x < 61:
            #x = time.time()
            screen1 = np.array(sct.grab(bbox))
            #x1 = time.time()
            screen2 = np.array(sct.grab(bbox))
            #x2 = time.time()
            screen_diff = cv2.absdiff(screen1,screen2)
            #x3 = time.time()
            diff = np.sum(screen_diff)
            #y = time.time()
            #print y-x
            #print diff
            #print(psutil.cpu_percent())
            cpu.append(psutil.cpu_percent())
            memory = psutil.virtual_memory()
            #print(memory.percent)
            mem.append(memory.percent)
            #time.sleep(.05)
            #if diff >= threshold:
            #    print "moving"
            #else:
            #    print "still"
            """cv2.imshow("hello",cv2.cvtColor(screen_diff, cv2.COLOR_BGR2RGB))
            if cv2.waitKey(25) & 0xFF == ord('q'):
                cv2.destroyAllWindows()
                break"""
        for item in cpu:
            file.write("%s, " %item)
        file.write("\n\n")
        for item2 in mem:
            file.write("%s, " %item2)
