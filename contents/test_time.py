import numpy as np
import cv2
from mss.windows import MSS as mss
import time
import pyautogui, sys

threshold = 100000

def screenDiff():
    time1 = []
    time2 = []
    time3 = []
    timeTotal = []
    file = open('data.txt', 'w')
    with mss() as sct:
        bbox = {'top':218, 'left':240, 'width':666, 'height':62}
        while(True):
            x = time.time()
            screen1 = np.array(sct.grab(bbox))
            x1 = time.time()
            screen2 = np.array(sct.grab(bbox))
            x2 = time.time()
            screen_diff = cv2.absdiff(screen1,screen2)
            x3 = time.time()
            diff = np.sum(screen_diff)
            y = time.time()
            t1 = x1 - x
            t2 = x2 - x1
            t3 = x3 - x2
            t4 = y - x
            time1.append(t1)
            time2.append(t2)
            time3.append(t3)
            timeTotal.append(t4)
            #print str(t1) + " :time first screenshot"
            #print str(t2) + " :time second screenshot"
            #print str(t3) + " :time image difference calculation"
            #print str(t4) + " :total time"
            #print diff
        #if diff >= threshold:
        #    print "moving"
        #else:
        #    print "still"
            cv2.imshow("hello",cv2.cvtColor(screen_diff, cv2.COLOR_BGR2RGB))
            if cv2.waitKey(25) & 0xFF == ord('q'):
                cv2.destroyAllWindows()
                break
            
        for item in time1:
            file.write("%s, " %item)
        file.write("\n\n")
        for item in time2:
            file.write("%s, " %item)
        file.write("\n\n")
        for item in time3:
            file.write("%s, " %item)
        file.write("\n\n")
        for item in timeTotal:
            file.write("%s, " %item)
        file.close()

def sessionE1():
    i = 0
    x = time.time()
    time.sleep(10)
    print time.time() - x

def pixelPos():
    try:
        while(True):
            x,y = pyautogui.position()
            positionStr = 'X: ' + str(x) + ' Y: ' + str(y)
            print positionStr
            sys.stdout.flush()
    except KeyboardInterrupt:
        print '\n'
