# Flyclicker

The flyclicker is a python script that uses the pyautogui module to click the mouse based on the detection of motion. Motion is detected by taking two consecutive screenshots and comparing them. The absolute value of the difference between these two screen shots will highlight where movement is occuring. The sum of the absolute value of the difference is then compared to a threshold value, which is assigned to prevent "noise" from being counted as movement. Depending on whether the obtained sum is greater or lower than the threshold value (whether movement is occuring or not), the script will manipulate the mouse and keyboard accordingly.

## Files

1. [flyclicker](contents/flyclicker.py)
2. [test memory](contents/test_memory.py): psutil module used to obtain cpu and memory usage
3. [test time](contents/test_time.py): used to obtain times certain events occured. pixelPos function was used to obtain locations of certain text boxes and buttons
4. [summary1](contents/vonCount.csv): summary of cpu and memory usage with sleeps
5. [summary2](contents/vonCount2.csv): summary of cpu and memory usage without sleeps
6. [time data](contents/data.csv): raw data and summary for times in the screenshot and image difference processes
7. [raw1](contents/raw1.txt): cpu and memory usage for just flyclicker
8. [raw2](contents/raw2.txt): cpu and memory usage including excel and youtube
9. [raw3](contents/raw3.txt): cpu and memory usage with excel and youtube, with 5 millisecond sleeps between each iteration
10. [raw4](contents/raw4.txt): 50 millisecond sleeps instead
11. [video](contents/rolling_cart_with_ball.mp4): video used to test motion detection; downloaded from [drjcslater](https://www.youtube.com/watch?v=JCampZIwL5w)
